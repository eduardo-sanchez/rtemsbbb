/**
 * @file
 *
 * @ingroup arm_beagle
 *
 * @brief Console configuration.
 */

/*
 * Copyright (c) 2012 Claas Ziemke. All rights reserved.
 *
 *  Claas Ziemke
 *  Kernerstrasse 11
 *  70182 Stuttgart
 *  Germany
 *  <claas.ziemke@gmx.net>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.com/license/LICENSE.
 *
 * Modified by Ben Gras <beng@shrike-systems.com> to make
 * interrupt-driven uart i/o work for beagleboards; beaglebone support added.
 */

#include <libchip/serial.h>
#include <libchip/ns16550.h>

#include <bsp.h>
#include <bsp/irq.h>
#include <bsp/uart-output-char.h>

#define CONSOLE_UART_THR (*(volatile unsigned int *)BSP_CONSOLE_UART_BASE)
#define CONSOLE_UART_RHR (*(volatile unsigned int *)BSP_CONSOLE_UART_BASE)
#define CONSOLE_SYSC (*(volatile uint32_t *) (BSP_CONSOLE_UART_BASE + BEAGLE_UART_SYSC))
#define CONSOLE_SYSS (*(volatile uint32_t *) (BSP_CONSOLE_UART_BASE + BEAGLE_UART_SYSS))

#define TX_FIFO_E (1<<5)
#define RX_FIFO_E (1<<0)

static bool beagle_uart2_probe(int minor); /* Prototype for warning suppression  */


static uint8_t beagle_uart_get_register(uintptr_t addr, uint8_t offset)
{
  volatile uint32_t *reg = (volatile uint32_t *) addr;

  return (uint8_t) reg [offset];
}

static void beagle_uart_set_register(uintptr_t addr, uint8_t offset, uint8_t val)
{
  volatile uint32_t *reg = (volatile uint32_t *) addr;

  reg [offset] = val;
}

#if IS_AM335X
 
/**
 * @brief Enable clock for UART 2 module
 *  Use when enabling UART2 in BeagleBone Black.
*/

static void beagle_uart2_enable_module(void)
{
   /* * * * * * * * * * * * * * * * * * * */
   /* Enabling clock for UART2 Module */
   /* * * * * * * * * * * * * * * * * * * */

   /* Writing to MODULEMODE field of CM_PER_UART2_CLKCTRL register. */
    REG(CM_PER_BASE + BEAGLE_CM_PER_UART2_CLKCTRL) |=
          CM_MODULEMODE_ENABLE;

    /* Waiting for MODULEMODE field to reflect the written value. */
    while(CM_MODULEMODE_ENABLE !=
          (REG(CM_PER_BASE + BEAGLE_CM_PER_UART2_CLKCTRL) &
          CM_MODULEMODE_MASK));
}

/**
 * @brief Change GPIO pins to UART (Rx and Tx) 
 *  Use when enabling UART2 in BeagleBone Black.
*/

static void beagle_uart2_pinmux(void)
{
   /* * * * * * * * * * * * * * * * * * * */
   /*       Set pinmux for UART2          */
   /* * * * * * * * * * * * * * * * * * * */

  /* RXD Pin: select pull-up and set pin as rx active */
    REG(BEAGLE_CONTROL_MODULE_BASE_REG + BEAGLE_CONF_UART2_RXD) = 
         (BEAGLE_CONF_UART2_RXD_PUTYPESEL | 
          BEAGLE_CONF_UART2_RXD_RXACTIVE);

   /* TXD Pin: select pull-up */
    REG(BEAGLE_CONTROL_MODULE_BASE_REG + BEAGLE_CONF_UART2_TXD) = 
          BEAGLE_CONF_UART2_TXD_PUTYPESEL; 
}

/**
 * @brief Reset module 
 *  Use when enabling UART2 in BeagleBone Black.
*/

static void beagle_uart2_reset_module(void)
{
    /* Performing Software Reset of the module. */
    REG(BEAGLE_BASE_UART_2 + BEAGLE_UART_SYSC) |= (BEAGLE_UART_SYSC_SOFTRESET);

    /* Wait until the process of Module Reset is complete. */
    while(!(REG(BEAGLE_BASE_UART_2 + BEAGLE_UART_SYSS) & BEAGLE_UART_SYSS_RESETDONE));

}

/**
 * @brief Switch operating mode to "UART16x" 
 *  Use when enabling UART2 in BeagleBone Black.
*/

static void beagle_uart2_UART16x_operating_mode(void)
{ 
   /* Clearing the MODESELECT field in MDR1. */
   
    REG(BEAGLE_BASE_UART_2 + BEAGLE_UART_MDR1) &= ~(BEAGLE_UART_MDR1_MODE_SELECT);
    /* Programming the MODESELECT field in MDR1. */
    REG(BEAGLE_BASE_UART_2 + BEAGLE_UART_MDR1) |= (BEAGLE_UART16x_OPER_MODE & BEAGLE_UART_MDR1_MODE_SELECT);

}

/**
 * @brief Enable UART2 in BeagleBone Black.
 *  Enable clock, pin setup, reset module and set operating mode.
*/

static bool beagle_uart2_probe(int minor)
{

  /* Enabling clock for UART2 Module */
  beagle_uart2_enable_module();

  /* Set pinmux for UART2 */
  beagle_uart2_pinmux();
 
  /* Reset UART Module */ 
  beagle_uart2_reset_module();

  /* Switch to UART16x operating mode. */  
  beagle_uart2_UART16x_operating_mode();

  return true;
}

#endif

console_tbl Console_Configuration_Ports [] = {
    {
      .sDeviceName = "/dev/ttyS0",
      .deviceType = SERIAL_NS16550,
#if CONSOLE_POLLED	/* option to facilitate running the tests */
      .pDeviceFns = &ns16550_fns_polled,
#else
      .pDeviceFns = &ns16550_fns,
#endif
      .deviceProbe = NULL,
      .ulMargin = 16,
      .ulHysteresis = 8,
      .pDeviceParams = (void *) CONSOLE_BAUD,
      .ulCtrlPort1 = BSP_CONSOLE_UART_BASE,
      .ulDataPort = BSP_CONSOLE_UART_BASE,
      .ulIntVector = BSP_CONSOLE_UART_IRQ,
      .getRegister = beagle_uart_get_register,
      .setRegister = beagle_uart_set_register,
      .ulClock = UART_CLOCK,  /* 48MHz base clock */
    },

#if IS_AM335X
    {
      .sDeviceName = "/dev/ttyS1",
      .deviceType = SERIAL_NS16550,
#if CONSOLE_POLLED	/* option to facilitate running the tests */
      .pDeviceFns = &ns16550_fns_polled,
#else  /* ELSE: UART will use Interrupts */
      .pDeviceFns = &ns16550_fns,
#endif 
      .deviceProbe = beagle_uart2_probe,
      .ulMargin = 16,
      .ulHysteresis = 8,
      .pDeviceParams = (void *) CONSOLE_BAUD,
      .ulCtrlPort1 = BEAGLE_BASE_UART_2,
      .ulDataPort = BEAGLE_BASE_UART_2,
      .ulIntVector = OMAP3_UART2_IRQ,
      .getRegister = beagle_uart_get_register,
      .setRegister = beagle_uart_set_register,
      .ulClock = UART_CLOCK,  /* 48MHz base clock */
    },
#endif

};

#define BEAGLE_UART_COUNT ( sizeof( Console_Configuration_Ports ) \
                             / sizeof( Console_Configuration_Ports[ 0 ] ) )

unsigned long Console_Configuration_Count = BEAGLE_UART_COUNT;

static int init_needed = 1; // don't rely on bss being 0

static void beagle_console_init(void)
{
  if(init_needed) {
    const uint32_t div = UART_CLOCK / 16 / CONSOLE_BAUD;
    CONSOLE_SYSC = 2;
    while ((CONSOLE_SYSS & 1) == 0)
      ;
    if ((CONSOLE_LSR & (CONSOLE_LSR_THRE | CONSOLE_LSR_TEMT)) == CONSOLE_LSR_THRE) {
      CONSOLE_LCR = 0x83;
      CONSOLE_DLL = div;
      CONSOLE_DLM = (div >> 8) & 0xff;
      CONSOLE_LCR = 0x03;
      CONSOLE_ACR = 0x00;
    }

    while ((CONSOLE_LSR & CONSOLE_LSR_TEMT) == 0)
      ;

    CONSOLE_LCR = 0x80 | 0x03;
    CONSOLE_DLL = 0x00;
    CONSOLE_DLM = 0x00;
    CONSOLE_LCR = 0x03;
    CONSOLE_MCR = 0x03;
    CONSOLE_FCR = 0x07;
    CONSOLE_LCR = 0x83;
    CONSOLE_DLL = div;
    CONSOLE_DLM = (div >> 8) & 0xff;
    CONSOLE_LCR = 0x03;
    CONSOLE_ACR = 0x00;
    init_needed = 0;
  }
}

#define CONSOLE_THR8 (*(volatile uint8_t *) (BSP_CONSOLE_UART_BASE + 0x00))

static void uart_write_polled( char c )
{
  if(init_needed) beagle_console_init();

  while( ( CONSOLE_LSR & TX_FIFO_E ) == 0 )
    ;
  CONSOLE_THR8 = c;
}

static void _BSP_put_char( char c ) {
   uart_write_polled( c );
   if (c == '\n') {
       uart_write_polled('\r');
   }
}

static int _BSP_get_char(void)
{
  if ((CONSOLE_LSR & CONSOLE_LSR_RDR) != 0) {
    return CONSOLE_RBR;
  } else {
    return -1;
  }
}

BSP_output_char_function_type BSP_output_char = _BSP_put_char;

BSP_polling_getchar_function_type BSP_poll_char = _BSP_get_char;
